# vue-reciprocal-overlap

Single page vue component to visualize overlapping intervals.

# Dependencies

Only Vue.js, no other dependencies.

# Demo Data

`{
    "first": {
        "name": "Target",
        "start": 32142,
        "end": 32445
    },
    "second": {
        "name": "Source",
        "start": 32202,
        "end": 32605
    }
}`

# Examples
<img src="https://gitlab.com/seyit/vue-reciprocal-overlap/-/raw/main/img/example-ro-1.png"  height="80" />
<img src="https://gitlab.com/seyit/vue-reciprocal-overlap/-/raw/main/img/example-ro-2.png"  height="80" />
<img src="https://gitlab.com/seyit/vue-reciprocal-overlap/-/raw/main/img/example-ro-3.png"  height="80" />
<img src="https://gitlab.com/seyit/vue-reciprocal-overlap/-/raw/main/img/example-ro-4.png"  height="80" />

